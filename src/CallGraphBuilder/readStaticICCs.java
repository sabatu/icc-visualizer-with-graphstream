package CallGraphBuilder;

import InstrumentationSuite.StaticSRMatching;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Sabatu on 5/31/2017.
 */
public class readStaticICCs {

    private static String fileName = "C:/Users/Sabatu/Desktop/resultsOutput.txt";

    private static ArrayList<IntentType> returnedICCs = new ArrayList<>();

    protected readStaticICCs() {};

    protected ArrayList<IntentType> getStaticICCs()
    {


        int indexOfIntents = 0;
        ArrayList<IntentFilter> manifestFilters = new ArrayList<IntentFilter>();
        String potentialComponent = new String();
        boolean filterLine = false;

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            boolean processManifest = false;
            boolean processResults = false;
            boolean processData = false;


            while ((line = br.readLine()) != null) {


                //Process Manifest
                if (line.contains("*****Manifest*****")) {
                    processManifest = true;
                }

                //Process the manifest
                if (processManifest) {
                    line = line.trim();
                    if (line.startsWith("com.")) {
                        potentialComponent = line;
                    }

                    if (line.contains("Intent filter:")) {
                        manifestFilters.add(new IntentFilter(potentialComponent));
                    }

                    if (line.contains("Actions:")) {
                        int beginIndex = line.indexOf("[");
                        int endIndex = line.indexOf("]");

                        char[] Actions = line.substring(beginIndex + 1, endIndex + 1).toCharArray();


                        StringBuilder singleAction = new StringBuilder();
                        for (char c : Actions) {
                            if (c == ',' || c == ']') {
                                manifestFilters.get(manifestFilters.size() - 1).getActions().add(singleAction.toString());
                                singleAction = new StringBuilder();
                            } else {
                                singleAction.append(c);
                            }
                        }


                    }

                    if (line.contains("Categories:")) {
                        int beginIndex = line.indexOf("[");
                        int endIndex = line.indexOf("]");

                        char[] Categories = line.substring(beginIndex + 1, endIndex + 1).toCharArray();


                        StringBuilder singleCategories = new StringBuilder();
                        for (char c : Categories) {
                            if (c == ',' || c == ']') {
                                manifestFilters.get(manifestFilters.size() - 1).getCategories().add(singleCategories.toString());
                                singleCategories = new StringBuilder();
                            } else {
                                singleCategories.append(c);
                            }
                        }

                    }

                    if (line.contains("Data:")) {
                        processData = true;
                    }

                    if (processData && !line.equals("Data:")) {


                        char[] Data = line.toCharArray();
                        StringBuilder singleData = new StringBuilder();
                        for (char c : Data) {
                            if (c == ',') {
                                manifestFilters.get(manifestFilters.size() - 1).getData().add(singleData.toString());
                                singleData = new StringBuilder();
                            } else {
                                singleData.append(c);
                            }
                        }

                        manifestFilters.get(manifestFilters.size() - 1).getData().add(singleData.toString());
                        processData = false;
                    }

                    if (line.contains("*****Result*****")) {
                        processManifest = false;
                        processResults = true;
                    }

                }
                //Process the results
                if (processResults) {


                    //Process first line of IC3 analysis
                    if (line.contains("$r")) {

                        returnedICCs.add(indexOfIntents, new IntentType());

                        //Capture caller of ICC from first line
                        int callerEnd = line.indexOf("/", 0);
                        int callerBegin = 0;
                        int[] guesses = new int[1000];
                        int i = 0;

                        while (callerBegin < callerEnd) {
                            guesses[i] = line.indexOf(".", callerBegin);
                            callerBegin = guesses[i] + 1;
                            i += 1;

                        }
                        callerBegin = guesses[i - 2];

                        returnedICCs.get(indexOfIntents).setCaller((String) line.subSequence(callerBegin + 1, callerEnd));


                        //Capture callsite information of ICC

                        int callsiteEnd = line.indexOf("(", callerEnd);

                        returnedICCs.get(indexOfIntents).setCallsite((String) line.subSequence(callerEnd + 1, callsiteEnd));


                        if (StaticSRMatching.is_IntentSendingAPI(returnedICCs.get(indexOfIntents).getCallsite())) {
                            returnedICCs.get(indexOfIntents).setSendOrRecv("Sent");
                        } else {
                            returnedICCs.get(indexOfIntents).setSendOrRecv("Received");
                        }
                    } else if (line.contains("Components: ")) {
                        int beginIndex = line.indexOf("[");
                        int endIndex = line.indexOf("]");

                        returnedICCs.get(indexOfIntents).setComponent((String) line.subSequence(beginIndex + 1, endIndex));
                        indexOfIntents += 1;

                    } else {


                    }


                    if (line.contains("!@#$%^&*")) {
                        processResults = false;
                    }
                }
            }
        }catch(Exception e){e.printStackTrace();}
        return returnedICCs;
        }
    }



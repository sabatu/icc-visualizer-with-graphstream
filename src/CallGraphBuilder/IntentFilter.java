package CallGraphBuilder;

import java.util.ArrayList;

public class IntentFilter {

    private String parentComponent;
    private ArrayList<String> actions = new ArrayList<>();
    private ArrayList<String> data = new ArrayList<>();
    private ArrayList<String> categories = new ArrayList<>();

    public IntentFilter()
    {

    }

    public IntentFilter(String parentComponent)
    {
        this.parentComponent = parentComponent;
        //categories
    }

    public String getParentComponent() {
        return parentComponent;
    }

    public void setParentComponent(String parentComponent) {
        this.parentComponent = parentComponent;
    }

    public ArrayList<String> getActions() {
        return actions;
    }

    public void setActions(ArrayList<String> actions) {
        this.actions = actions;
    }

    public ArrayList<String> getData() {
        return data;
    }

    public void setData(ArrayList<String> data) {
        this.data = data;
    }

    public ArrayList<String> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<String> categories) {
        this.categories = categories;
    }


}

package CallGraphBuilder;

/**
 * Created by Sabatu on 7/5/2017.
 */

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;



/**
 * Created by Sabatu on 6/10/2017.
 */
public class TabbedLegendManager {

    private int InternalGridX = 0;
    private int InternalGridY = 0;
    private Font headerFont = new Font("SansSerif", Font.BOLD, 10);

    private JTabbedPane tabbedLegend = new JTabbedPane();
    private JPanel componentContainer = new JPanel();
    private JPanel callerContainer = new JPanel();




    private GridBagConstraints cs = new GridBagConstraints();

    public TabbedLegendManager()
    {

    }


    public TabbedLegendManager(JPanel GraphContainer, GridBagConstraints constr,int initialX,int initialY) {

        tabbedLegend.addTab("Component Statistics", componentContainer);
        tabbedLegend.addTab("Caller Statistics", callerContainer);

        componentContainer.setLayout(new GridBagLayout());

        cs = constr;

        cs.gridx = initialX;
        cs.gridy = initialY;
        cs.weightx = 0.5;
        cs.weighty = 0.5;

        callerContainer.setLayout(new GridBagLayout());

        cs = constr;

        cs.gridx = initialX;
        cs.gridy = initialY;
        cs.weightx = 0.5;
        cs.weighty = 0.5;


        GraphContainer.add(tabbedLegend, cs);

    }

    protected void setManager(JPanel GraphContainer, GridBagConstraints constr,int initialX,int initialY)
    {


        tabbedLegend.addTab("Component Statistics", componentContainer);
        tabbedLegend.addTab("Caller Statistics", callerContainer);

        componentContainer.setLayout(new GridBagLayout());

        cs = constr;

        cs.gridx = initialX;
        cs.gridy = initialY;
        cs.weightx = 0.5;
        cs.weighty = 0.5;

        callerContainer.setLayout(new GridBagLayout());

        cs = constr;

        cs.gridx = initialX;
        cs.gridy = initialY;
        cs.weightx = 0.5;
        cs.weighty = 0.5;


        GraphContainer.add(tabbedLegend, cs);


    }

    protected void addHeader(String[] headerFields, String type)
    {
        for (String header : headerFields)
        {
            JTextField headerText = new JTextField();
            headerText.setText(header);
            headerText.setEditable(false);
            headerText.setFont(headerFont);
            headerText.setHorizontalAlignment(JTextField.CENTER);

            cs.gridx = InternalGridX;
            cs.gridy = InternalGridY;

            if(type.equals("Component"))
            {
                componentContainer.add(headerText, cs);
            }
            else if(type.equals("Caller"))
            {
                callerContainer.add(headerText, cs);
            }



            InternalGridX += 1;
        }

        InternalGridX = 0;
        InternalGridY = 1;
    }

    protected ArrayList<JTextField> addTypeField(Color legendColor, String legendShape, String description, String type)
    {

        ArrayList<JTextField> statsFields = new ArrayList<>();

        JPanel newVisualDescriptor = new JPanel() {

            protected void paintComponent(Graphics g) {

                g.setColor(legendColor);

                if(legendShape.equals("Rectangle"))
                {
                    g.fillRect(15, 15, 40, 40);
                }
                else if(legendShape.equals("Circle"))
                {
                    g.fillOval(15, 15, 40, 40);
                }
                else if(legendShape.equals("Line"))
                {
                    g.fillRect(0, 35, 80, 2);
                }
            }; //<=== End of paintComponent
        }; //<== End of JPanel newfield Constructor

        cs.fill = GridBagConstraints.BOTH;
        cs.weightx = 0.5;
        cs.weighty = 0.5;
        cs.gridx = InternalGridX;
        cs.gridy = InternalGridY;

        newVisualDescriptor.setBorder(BorderFactory.createLineBorder(new Color(184,207,229)));

        if(type.equals("Caller"))
        {
            callerContainer.add(newVisualDescriptor, cs);
        }
        else if(type.equals("Component"))
        {
            componentContainer.add(newVisualDescriptor,cs);
        }

        InternalGridX += 1;

        JTextField newTextDescription = new JTextField();
        newTextDescription.setText(description);
        newTextDescription.setEditable(false);
        newTextDescription.setFont(headerFont);
        newTextDescription.setHorizontalAlignment(JTextField.CENTER);

        cs.gridx = InternalGridX;
        cs.gridy = InternalGridY;

        if(type.equals("Caller"))
        {
            callerContainer.add(newTextDescription, cs);
        }
        else if(type.equals("Component"))
        {
            componentContainer.add(newTextDescription, cs);
        }

        InternalGridX += 1;

        JTextField fieldCounter = new JTextField();
        fieldCounter.setText("0");
        fieldCounter.setEditable(false);
        fieldCounter.setFont(headerFont);
        fieldCounter.setHorizontalAlignment(JTextField.CENTER);

        cs.gridx = InternalGridX;
        cs.gridy = InternalGridY;

        if(type.equals("Caller"))
        {
            callerContainer.add(fieldCounter, cs);
        }
        else if(type.equals("Component"))
        {
            componentContainer.add(fieldCounter, cs);
        }

        InternalGridX += 1;

        JTextField percentCounter = new JTextField();
        percentCounter.setText("0 %");
        percentCounter.setEditable(false);
        percentCounter.setFont(headerFont);
        percentCounter.setHorizontalAlignment(JTextField.CENTER);

        cs.gridx = InternalGridX;
        cs.gridy = InternalGridY;

        if(type.equals("Caller"))
        {
            callerContainer.add(percentCounter, cs);
        }
        else if(type.equals("Component"))
        {
            componentContainer.add(percentCounter, cs);
        }

        InternalGridX = 0;
        InternalGridY += 1;

        statsFields.add(fieldCounter);
        statsFields.add(percentCounter);

        return statsFields;

    }







    public void repaintLegend()
    {
        callerContainer.repaint();
    }




}

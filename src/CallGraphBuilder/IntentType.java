package CallGraphBuilder;

/**
 * Created by Sabatu on 4/19/2017.
 */
public class IntentType {

    private  String ID;
    private  String type;
    private  String caller;
    private  String callsite;
    private  String action;
    private  String datastring;
    private  String datauri;
    private  String scheme;
    private  String flags;
    private  Object vertexID;
    private  String extras;
    private  String component;
    private  String timestamp;
    private String packagename;
    private String sendOrRecv;
    private String lineNumber;
    private String componentOfClass;
    private String label;
    private int x;
    private int y;
    private int nodeID;


    // constructor

    protected IntentType() {
        this.ID = new String();
        this.type = new String();
        this.caller = new String();
        this.callsite = new String();
        this.action = new String();
        this.packagename = new String();
        this.datastring = new String();
        this.datauri = new String();
        this.scheme = new String();
        this.flags = new String();
        this.vertexID = new String();
        this.extras = new String();
        this.component = new String();
        this.timestamp = new String();
        this.sendOrRecv = new String();
        this.lineNumber = new String();
        this.componentOfClass = new String();
        this.label = new String();
        this.x = 0;
        this.y = 0;
        this.nodeID = 0;

    }
    protected IntentType(String ID) {
        this.ID = ID;
        this.type = new String();
        this.caller = new String();
        this.callsite = new String();
        this.action = new String();
        this.packagename = new String();
        this.datastring = new String();
        this.datauri = new String();
        this.scheme = new String();
        this.flags = new String();
        this.vertexID = new String();
        this.extras = new String();
        this.component = new String();
        this.timestamp = new String();
        this.sendOrRecv = new String();
        this.lineNumber = new String();
        this.componentOfClass = new String();
        this.label = new String();
        this.x = 0;
        this.y = 0;
        this.nodeID = 0;

    }

    // getters
    protected  String getID() { return ID; }
    protected  String getType() { return type; }
    protected  String getCaller() { return caller; }
    protected  String getCallsite() { return callsite; }
    protected  String getAction() { return action; }
    protected  String getPackageName() { return packagename; }
    protected  String getdataString() { return datastring; }
    protected  String getdataURI() { return datauri; }
    protected  String getScheme() { return scheme; }
    protected  String getFlags() { return flags; }
    protected  Object getvertexID() { return vertexID; }
    protected  String getExtras() { return extras; }
    protected  String getComponent() { return component; }
    protected  String getTimestamp() { return timestamp; }
    protected String  getSendOrRecv() {return sendOrRecv;}
    protected String getlineNumber() {return lineNumber;}
    protected String getcomponentOfClass() {return componentOfClass;}
    protected String getLabel() {return label;}
    protected int getX() {return x;}
    protected int getY() {return y;}
    protected int getnodeID() {return nodeID;}





    // setter

  protected void setID(String ID) { this.ID = ID; }
    protected void setType(String type) { this.type = type; }
    protected void setCaller(String caller) { this.caller = caller; }
    protected void setCallsite(String callsite) { this.callsite = callsite; }
    protected void setAction(String action) { this.action = action; }
    protected void setPackageName(String packagename) { this.packagename = packagename; }
    protected void setdataString(String datastring) { this.datastring = datastring; }
    protected void setdataURI(String datauri) { this.datauri = datauri; }
    protected void setScheme(String scheme) { this.scheme = scheme; }
    protected void setFlags(String flags) { this.flags = flags; }
    protected void setvertexID(Object vertexID) { this.vertexID = vertexID; }
    protected void setExtras(String extras) { this.extras = extras; }
    protected void setComponent(String component) { this.component = component; }
    protected void setTimestamp(String timestamp) { this.timestamp = timestamp; }
    protected void setSendOrRecv(String sendOrRecv) {this.sendOrRecv = sendOrRecv;}
    protected void setlineNumber(String lineNumber) {this.lineNumber = lineNumber;}
    protected void setcomponentOfClass(String componentOfClass) {this.componentOfClass = componentOfClass;}
    protected void setLabel(String label) {this.label = label;}
    protected void setX(int x) {this.x = x;}
    protected void setY(int y) {this.x = x;}
    protected void setnodeID(int nodeID) {this.nodeID = nodeID;}



    //This function is used to display verbose ICC information when the mouse is hovered over an ICC node,
    // and is called from mouseMoved() in the MouseMotionEvent Manager.

    protected static String postIntentInfo(IntentType selectedIntent)
    {

        String windowOutput = new String();

        windowOutput += "Action: " + selectedIntent.getAction() + "\n";
        windowOutput += "Caller: " + selectedIntent.getCaller() + "\n";
        windowOutput += "Callsite: " + selectedIntent.getCallsite() + "\n";
        windowOutput += "Component: " + selectedIntent.getComponent() + "\n";
        windowOutput += "Data String: " + selectedIntent.getdataString() + "\n";
        windowOutput += "Data URI: " + selectedIntent.getdataURI() + "\n";
        windowOutput += "Extras: " + selectedIntent.getExtras() + "\n";
        windowOutput += "Flags: " + selectedIntent.getFlags() + "\n";
        windowOutput += "Line number: " + selectedIntent.getlineNumber() + "\n";
        windowOutput += "Package Name: " + selectedIntent.getPackageName() + "\n";
        windowOutput += "Scheme: " + selectedIntent.getScheme() + "\n";
        windowOutput += "Timestamp: " + selectedIntent.getID() + "\n";
        windowOutput += "Type: " + selectedIntent.getType() + "\n";
        windowOutput += "Sent or Received? " + selectedIntent.getSendOrRecv() + "\n";
        windowOutput += "Component Of Class: " + selectedIntent.getcomponentOfClass() + "\n";

        return windowOutput;
    }


    /*This compare checks specific fields of the currently instantiated IntentType against a comparison IntentType,
    * in order to determine if the two ICCs are equivalent communications made at different points in time.*/
    protected boolean compare(IntentType comp)
    {

        boolean equivalent = false;

        if(this.caller == comp.caller &&
                this.callsite == comp.callsite &&
                this.action == comp.action &&
                this.datastring == comp.datastring &&
                this.datauri == comp.datauri &&
                this.scheme == comp.scheme &&
                this.flags == comp.flags &&
                this.extras == comp.extras &&
                this.component == comp.component &&
                this.packagename == comp.packagename &&
                this.sendOrRecv == comp.sendOrRecv &&
                this.componentOfClass == comp.componentOfClass)
        {
            equivalent = true;
        }

        return equivalent;






    }




}











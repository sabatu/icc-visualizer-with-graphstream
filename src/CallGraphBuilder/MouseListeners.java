package CallGraphBuilder;

import org.graphstream.graph.Graph;
import org.graphstream.ui.geom.Point3;
import org.graphstream.ui.graphicGraph.GraphicElement;
import org.graphstream.ui.view.Camera;
import org.graphstream.ui.view.View;
import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.util.DefaultMouseManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import static java.awt.Color.*;

/**
 * Created by Sabatu on 7/7/2017.
 */
public class MouseListeners {

    //These variables help compute the offsets for the mouse's zoom/pan functions
    protected final double MIN_VIEW_PERCENT = 0.01;
    protected final double MAX_VIEW_PERCENT = 4;
    protected final double VIEW_PERCENT_STEP = 0.025;
    protected Point3 previousPoint;
    protected Point3 previousPoint2;
    protected Camera dynamicViewCam;


    protected void DynamicGraphMouseListener(View dynGraphView, Map callerGroupEdges, Map callerIDreferences,
                                           View dynGraphViewClone, Graph dynamicGraph, Map callerGroupNodes)
    {

        //Dynamic Graph Views Mouse Listener

        dynGraphView.addMouseListener(new DefaultMouseManager()
        {

            @Override
            public void mouseClicked(MouseEvent e) {

                if(e.isControlDown()) {
                    centerOn(e.getPoint());
                }

                //The following block of code handles generating (or removing) caller edges, when a caller node gets clicked on.

                GraphicElement currentElement = dynGraphViewClone.findNodeOrSpriteAt(e.getX(), e.getY());

                String selectedElement = currentElement.toString();

                //If true, then caller ID node has been clicked on
                if(Integer.parseInt(selectedElement) >= 2500000 && Integer.parseInt(selectedElement) < 3000000)
                {
                    //Edges are currently on, so remove them
                    if(callerGroupEdges.containsKey(selectedElement))
                    {
                        ArrayList<String> edgeRefs = (ArrayList<String>)callerGroupEdges.get(selectedElement);

                        Iterator<String> edgeRefsIT = edgeRefs.iterator();

                        while(edgeRefsIT.hasNext())
                        {
                            dynamicGraph.removeEdge(edgeRefsIT.next());
                        }

                        callerGroupEdges.remove(selectedElement);

                    }
                    //Edges are currently off, so add them
                    else
                    {
                        //Get caller from node ID reference
                        String caller = (String)callerIDreferences.get(selectedElement);

                        //Retrieve all nodes that caller has instantiated
                        ArrayList<String> referencedNodes = (ArrayList<String>)callerGroupNodes.get(caller);

                        Iterator<String> refNodesIT = referencedNodes.iterator();

                        //create array of edge reference IDs for later potential use in removal of edges
                        ArrayList<String> edgeReferences = new ArrayList<String>();

                        while (refNodesIT.hasNext()) {
                            //Generate unique ID for added edges
                            Random random = new Random();

                            String currentToNode = refNodesIT.next();

                            //this if else conditional ensures that the selected node get an edge drawn to itself
                            if(selectedElement.equals(currentToNode))
                            {
                                continue;
                            }
                            else
                            {
                                dynamicGraph.addEdge(random.toString(), selectedElement, currentToNode, true);

                                //Store unique edge
                                edgeReferences.add(random.toString());
                            }

                        }

                        //Save caller - edge references for later potential removal of edges
                        callerGroupEdges.put(selectedElement, edgeReferences);
                    }

                }


            }


            private void centerOn(Point point) {
                Camera camera = dynGraphView.getCamera();
                int x = point.x;
                int y = point.y;
                Point3 newCenter = camera.transformPxToGu(x, y);
                camera.setViewCenter(newCenter.x, newCenter.y, 0);
                previousPoint = newCenter;
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                previousPoint = point3(e.getPoint());
            }



            @Override
            public void mousePressed(MouseEvent e) {
                boolean isShiftPressed = e.isShiftDown();


                if(!isShiftPressed) {
                    curElement = dynGraphViewClone.findNodeOrSpriteAt(e.getX(), e.getY());


                    // if (curElement != null) {
                    //      mouseButtonPressOnElement(curElement, e);
                    //  }

                    previousPoint = point3(e.getPoint());
                }

                curElement = dynGraphViewClone.findNodeOrSpriteAt(e.getX(), e.getY());

                // if (curElement != null) {
                //     mouseButtonPressOnElement(curElement, e);
                // }
                // else {
                x1 = e.getX();
                y1 = e.getY();
                // mouseButtonPress(e);
                dynGraphViewClone.beginSelectionAt(x1, y1);
                //   }
            }



            @Override
            public void mouseReleased(MouseEvent e) {

                boolean isShiftPressed = e.isShiftDown();
                if(!isShiftPressed) {
                    //  if (curElement != null) {
                    //       mouseButtonReleaseOffElement(curElement, e);
                    //       curElement = null;
                    //      }
                    return;
                }

                if (curElement != null) {
                    mouseButtonReleaseOffElement(curElement, e);
                    curElement = null;
                } else {
                    float x2 = e.getX();
                    float y2 = e.getY();
                    float t;

                    if (x1 > x2) {
                        t = x1;
                        x1 = x2;
                        x2 = t;
                    }
                    if (y1 > y2) {
                        t = y1;
                        y1 = y2;
                        y2 = t;
                    }

                    mouseButtonRelease(e, dynGraphView.allNodesOrSpritesIn(x1, y1, x2, y2));
                    dynGraphView.endSelectionAt(x2, y2);
                }

                boolean nodesWereSelected = false;
                System.out.println("## Selection of Nodes is ...");



                if(!nodesWereSelected) {
                    System.out.println("EMPTY.");
                }
            }



            private Point3 point3(Point point) {
                Camera camera = dynGraphViewClone.getCamera();
                return camera.transformPxToGu(point.x, point.y);
            }



        });

    } //<=== End of Dynamic Graph Mouse Listener


        protected void DynamicGraphMouseMotionListener(View dynGraphView, View dynGraphViewClone, ArrayList<IntentType> intentStorage,
                                                       GraphDesignSpec styles, ArrayList<String> matchedInfo, Viewer dynGraphViewer)
        {
            dynGraphView.addMouseMotionListener(new DefaultMouseManager()

            {
                boolean nodeMatched = false;
                boolean windowUp = false;
                JFrame detailedInfoFrame = new JFrame();


                public void mouseMoved(MouseEvent e)
                {

                    GraphicElement currentElement = dynGraphViewClone.findNodeOrSpriteAt(e.getX(), e.getY());

                    //Current Element is not a node, so don't care about it
                    if(currentElement == null)
                    {
                        nodeMatched = false;
                    }

                    //Current element is a node, so changed the nodeMatched boolean to true.
                    if(currentElement != null && (nodeMatched == false))
                    {
                        nodeMatched = true;
                    }

                    //This is where the detailed information window gets posted
                    if((nodeMatched == true) && (windowUp == false) && (dynamicViewCam.getViewPercent() <= .075) )
                    {
                        //This function call (implemented below) finds a matching intent based off the label returned by the mouse hover event

                        String selectedElement = currentElement.toString();
                        int selectedElementID = Integer.parseInt(selectedElement);

                        /*This conditional separates out internal/external nodes, as different information will be
                        /posted depending on which type of node is being hovered over by the mouse
                        The filter below specifically searches for ICC nodes*/

                        if (selectedElementID >= 500000 && selectedElementID < 1000000) {

                            IntentType returnedIntent = (intentStorage.stream().filter(line -> Integer.toString(line.getnodeID()).equals(selectedElement)).findFirst()).get();

                            //This function call (implemented below) posts a new Jframe with the returned intent
                            String windowOutput = IntentType.postIntentInfo(returnedIntent);

                            Dimension DEFAULT_SIZE = new Dimension(250, 250);

                            //Textarea for the verbose information popup window
                            JTextArea textarea = new JTextArea(10, 25);
                            textarea.setFont(styles.getDetailWindowFont());
                            textarea.setForeground(Color.WHITE);
                            textarea.setLineWrap(true);
                            textarea.setOpaque(false);
                            textarea.setBackground(new Color(red.getRed(), green.getGreen(), blue.getBlue(), 0));
                            textarea.setText(windowOutput);

                            //Jframe for the verbose intent info pop-up
                            detailedInfoFrame = new JFrame();

                            //These settings make the background transparent for the pop-up intent info
                            detailedInfoFrame.setUndecorated(true);
                            detailedInfoFrame.setBackground(new Color(0, 255, 0, 0));
                            detailedInfoFrame.setContentPane(new ContentPane());
                            detailedInfoFrame.getContentPane().setBackground(Color.BLACK);
                            detailedInfoFrame.setLayout(new BorderLayout());
                            detailedInfoFrame.getContentPane().add(textarea, BorderLayout.LINE_START);
                            detailedInfoFrame.setDefaultCloseOperation((WindowConstants.DISPOSE_ON_CLOSE));

                            //Get the nodes exact coordinates(converted into pixels) and use the coordinates to set the pop up windows location
                            Point3 hoveredNodeCoordinates =  dynGraphViewer.getDefaultView().getCamera().transformGuToPx(currentElement.getX(),currentElement.getY(),0.0);

                            detailedInfoFrame.setLocation((int)hoveredNodeCoordinates.x, (int)hoveredNodeCoordinates.y);
                            detailedInfoFrame.resize(DEFAULT_SIZE);
                            detailedInfoFrame.setVisible(true);

                            windowUp = true;

                        }

                        //This else conditional handles the pop-up window for "externally matched" nodes
                        else if(selectedElementID >= 0 && selectedElementID < 500000)
                        {
                            Dimension DEFAULT_SIZE = new Dimension(250, 110);

                            //Textarea for the verbose information popup window
                            JTextArea textarea = new JTextArea(10, 25);

                            textarea.setFont(styles.getDetailWindowFont());
                            textarea.setForeground(Color.WHITE);
                            textarea.setLineWrap(true);
                            textarea.setWrapStyleWord(true);

                            textarea.setOpaque(false);
                            textarea.setBackground(new Color(red.getRed(), green.getGreen(), blue.getBlue(), 0));

                            textarea.setText(matchedInfo.get(Integer.parseInt(currentElement.toString())));

                            //Jframe for the verbose intent info pop-up
                            detailedInfoFrame = new JFrame();

                            //These settings make the background transparent for the pop-up intent info
                            detailedInfoFrame.setUndecorated(true);
                            detailedInfoFrame.setBackground(new Color(0, 255, 0, 0));
                            detailedInfoFrame.setContentPane(new ContentPane());
                            detailedInfoFrame.getContentPane().setBackground(Color.BLACK);
                            detailedInfoFrame.setLayout(new BorderLayout());
                            detailedInfoFrame.getContentPane().add(textarea, BorderLayout.LINE_START);
                            detailedInfoFrame.setDefaultCloseOperation((WindowConstants.DISPOSE_ON_CLOSE));

                            //Get the nodes exact coordinates(converted into pixels) and use the coordinates to set the pop up windows location
                            Point3 hoveredNodeCoordinates =  dynGraphViewer.getDefaultView().getCamera().transformGuToPx(currentElement.getX(),currentElement.getY(),0.0);

                            detailedInfoFrame.setLocation((int)hoveredNodeCoordinates.x, (int)hoveredNodeCoordinates.y);
                            detailedInfoFrame.resize(DEFAULT_SIZE);
                            detailedInfoFrame.setVisible(true);

                            windowUp = true;

                        }
                        else{
                            //do nothing
                        }
                    }

                    //Mouse has left the node with generated detailed information JFrame, so dispose of the frame and clean up
                    if((nodeMatched == false) && (windowUp == true))
                    {
                        detailedInfoFrame.setVisible(false);
                        detailedInfoFrame.dispose();

                        windowUp = false;
                    }
                }



                @Override
                public void mouseDragged(MouseEvent e) {
                    boolean isShiftPressed = e.isShiftDown();
                    if(!isShiftPressed) {


                        if (curElement != null) {
                            elementMoving(curElement, e);
                        }

                        double xDelta = e.getX();
                        double yDelta = e.getY();
                        Point3 currentPoint = point3(e.getPoint());


                        //  if(previousPoint == null)
                        //  {


                        //       xDelta = currentPoint.x;
                        //       yDelta = currentPoint.y;
                        //     }
                        //   else if(previousPoint != null) {

                        xDelta = (currentPoint.x - previousPoint.x) / 1.5;
                        yDelta = (currentPoint.y - previousPoint.y) / 1.5;
                        //   }
                        pan(xDelta, yDelta);

                        previousPoint = currentPoint;
                    } else {
                        if (curElement != null) {
                            elementMoving(curElement, e);
                        } else {
                            dynGraphViewClone.selectionGrowsAt(e.getX(), e.getY());

                            //        System.out.println(e.getPoint().toString());
                        }
                    }
                }

                private void pan(double xDelta, double yDelta) {
                    Camera camera = dynGraphViewClone.getCamera();
                    Point3 point = camera.getViewCenter();
                    double x = point.x - xDelta;
                    double y = point.y - yDelta;
                    camera.setViewCenter(x, y, 0);
                }



                private Point3 point3(Point point) {
                    Camera camera = dynGraphViewClone.getCamera();
                    return camera.transformPxToGu(point.x, point.y);
                }


            });



        } //<== End of dynamic graph mouse motion wheel listener

    protected void DynamicGraphMouseWheelListener(JPanel panel1, View dynGraphView)
    {
        //dynamic graphs mouse wheel listener
        panel1.addMouseWheelListener(new MouseWheelListener()      {


            public void mouseWheelMoved(MouseWheelEvent e) {

                double rotation = e.getPreciseWheelRotation();

                dynamicViewCam = dynGraphView.getCamera();
                // +ve is zoom out
                // -ve is zoom in.
                double viewPercent = dynamicViewCam.getViewPercent();
                if(rotation < 0) { // zoom in
                    viewPercent = viewPercent - VIEW_PERCENT_STEP;
                    viewPercent =
                            (viewPercent <= MIN_VIEW_PERCENT) ? MIN_VIEW_PERCENT
                                    : viewPercent;
                } else { // zoom out
                    viewPercent = viewPercent + VIEW_PERCENT_STEP;
                    viewPercent =
                            (viewPercent >= MAX_VIEW_PERCENT) ? MAX_VIEW_PERCENT
                                    : viewPercent;
                }
                System.out.println("View percent: " + viewPercent);
                dynamicViewCam.setViewPercent(viewPercent);

            }        });

    }


    protected void StaticMouseListener(View staticView, View staticViewClone)
    {
        //Static Graphs mouse manager
        staticView.addMouseListener(new DefaultMouseManager()

        {
            @Override
            public void mouseClicked(MouseEvent e) {

                if(e.isControlDown()) {
                    centerOn(e.getPoint());
                }
            }

            private void centerOn(Point point) {
                Camera camera = staticView.getCamera();
                int x = point.x;
                int y = point.y;
                Point3 newCenter = camera.transformPxToGu(x, y);
                camera.setViewCenter(newCenter.x, newCenter.y, 0);
                previousPoint2 = newCenter;


            }

            @Override
            public void mouseEntered(MouseEvent e) {
                previousPoint2 = point3(e.getPoint());
            }

            @Override
            public void mousePressed(MouseEvent e) {
                boolean isShiftPressed = e.isShiftDown();
                if(!isShiftPressed) {
                    curElement = staticViewClone.findNodeOrSpriteAt(e.getX(), e.getY());
                    if (curElement != null) {
                        mouseButtonPressOnElement(curElement, e);
                    }

                    previousPoint2 = point3(e.getPoint());
                }

                curElement = staticViewClone.findNodeOrSpriteAt(e.getX(), e.getY());

                if (curElement != null) {
                    mouseButtonPressOnElement(curElement, e);
                } else {
                    x1 = e.getX();
                    y1 = e.getY();
                    mouseButtonPress(e);
                    staticViewClone.beginSelectionAt(x1, y1);
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {

                boolean isShiftPressed = e.isShiftDown();
                if(!isShiftPressed) {
                    if (curElement != null) {
                        mouseButtonReleaseOffElement(curElement, e);
                        curElement = null;
                    }
                    return;
                }

                if (curElement != null) {
                    mouseButtonReleaseOffElement(curElement, e);
                    curElement = null;
                } else {
                    float x2 = e.getX();
                    float y2 = e.getY();
                    float t;

                    if (x1 > x2) {
                        t = x1;
                        x1 = x2;
                        x2 = t;
                    }
                    if (y1 > y2) {
                        t = y1;
                        y1 = y2;
                        y2 = t;
                    }

                    mouseButtonRelease(e, staticView.allNodesOrSpritesIn(x1, y1, x2, y2));
                    staticView.endSelectionAt(x2, y2);
                }

                boolean nodesWereSelected = false;
                System.out.println("## Selection of Nodes is ...");



                if(!nodesWereSelected) {
                    System.out.println("EMPTY.");
                }
            }

            private Point3 point3(Point point) {
                Camera camera = staticViewClone.getCamera();
                return camera.transformPxToGu(point.x, point.y);
            }

        });
    }

    protected void staticViewMouseMotionListener(View staticView, View staticViewClone)
    {
        staticView.addMouseMotionListener(new DefaultMouseManager()

        {
            @Override
            public void mouseDragged(MouseEvent e) {
                boolean isShiftPressed = e.isShiftDown();
                if(!isShiftPressed) {


                    if (curElement != null) {
                        elementMoving(curElement, e);
                    }

                    double xDelta = e.getX();
                    double yDelta = e.getY();
                    Point3 currentPoint = point3(e.getPoint());



                    //  if(previousPoint == null)
                    //  {


                    //       xDelta = currentPoint.x;
                    //       yDelta = currentPoint.y;
                    //     }
                    //   else if(previousPoint != null) {

                    xDelta = (currentPoint.x - previousPoint2.x) / 1.5;
                    yDelta = (currentPoint.y - previousPoint2.y) / 1.5;
                    //   }
                    pan(xDelta, yDelta);

                    previousPoint2 = currentPoint;
                } else {
                    if (curElement != null) {
                        elementMoving(curElement, e);
                    } else {
                        staticViewClone.selectionGrowsAt(e.getX(), e.getY());

                        //        System.out.println(e.getPoint().toString());
                    }
                }
            }

            private void pan(double xDelta, double yDelta) {
                Camera camera = staticViewClone.getCamera();
                Point3 point = camera.getViewCenter();
                double x = point.x - xDelta;
                double y = point.y - yDelta;
                camera.setViewCenter(x, y, 0);
            }



            private Point3 point3(Point point) {
                Camera camera = staticViewClone.getCamera();
                return camera.transformPxToGu(point.x, point.y);
            }




        });

    }


    protected void staticViewMouseWheelListener(JPanel panel2, View staticView)
    {
        panel2.addMouseWheelListener(new MouseWheelListener()      {


            public void mouseWheelMoved(MouseWheelEvent e) {

                double rotation = e.getPreciseWheelRotation();

                Camera cam = staticView.getCamera();
                // +ve is zoom out
                // -ve is zoom in.
                double viewPercent = cam.getViewPercent();
                if(rotation < 0) { // zoom in
                    viewPercent = viewPercent - VIEW_PERCENT_STEP;
                    viewPercent =
                            (viewPercent <= MIN_VIEW_PERCENT) ? MIN_VIEW_PERCENT
                                    : viewPercent;
                } else { // zoom out
                    viewPercent = viewPercent + VIEW_PERCENT_STEP;
                    viewPercent =
                            (viewPercent >= MAX_VIEW_PERCENT) ? MAX_VIEW_PERCENT
                                    : viewPercent;
                }
                cam.setViewPercent(viewPercent);
            }        });

    }


}

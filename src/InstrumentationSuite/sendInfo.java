package InstrumentationSuite;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by Sabatu on 4/22/2017.
 */
public class sendInfo implements Runnable {


        private String info;


        public sendInfo(String msg)
        {
            info = msg;
        }

        public void run() {
            try {

                 Socket client;
                 PrintWriter printwriter;

                client = new Socket("10.0.2.2", 7575);  //connect to server
                printwriter = new PrintWriter(client.getOutputStream(),true);
                printwriter.write(info);  //write the message to output stream

                printwriter.flush();
                printwriter.close();
                client.close();   //closing the connection

            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {


                e.printStackTrace();
            }
        }

}

package InstrumentationSuite;

import android.content.Intent;

import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by Sabatu on 3/11/2017.
 */
public class logcatOutputINSTR {

    /** Used to avoid infinite recursion */
    private static boolean active = false;

    private Socket client;
    private PrintWriter printwriter;

    private static logicClock g_lgclock = null;
    public static void installClock (logicClock lgclock) {
        g_lgclock = lgclock;
    }

    public synchronized static void dumpIntentInfo(Intent itn, String caller, String stmtIntentFoundAt,String CmpntOfClass, String sentOrRecv, String lineNumber) {
        if (active) return;
        active = true;
        try {
            dumpIntentInfo_im(itn, caller, stmtIntentFoundAt, CmpntOfClass, sentOrRecv, lineNumber);
        } finally {
            active = false;
        }
    }

    public synchronized static void dumpIntentInfo_im(Intent capturedIntent, String method, String stmtIntentFoundAt,String CmpntOfClass, String sentOrRecv, String lineNumber) {
        try {

             Socket client;
            PrintWriter printwriter;

            android.util.Log.e("(ΨΩ∇)Intent Type: ", sentOrRecv);

            new Thread(new sendInfo("(ΨΩ∇)Intent Type: " + sentOrRecv)).start();

            android.util.Log.e("(αβγ)Intent Caller: ", method);

            new Thread(new sendInfo("(αβγ)Intent Caller: " + method)).start();

            android.util.Log.e("(δεζ)Component Type of caller: ", CmpntOfClass);

            new Thread(new sendInfo("(δεζ)Component Type of caller: " + CmpntOfClass)).start();

            android.util.Log.e("(ηθι)Intent Callsite: ", stmtIntentFoundAt);

            new Thread(new sendInfo("(ηθι)Intent Callsite: " + stmtIntentFoundAt)).start();

            android.util.Log.e("(πρς)Java Line Number: ", lineNumber);

            new Thread(new sendInfo("(πρς)Java Line Number: " + lineNumber)).start();







            dumpIntent(capturedIntent);

            if (g_lgclock != null) {
                g_lgclock.packClock(capturedIntent);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public synchronized static void dumpIntent(Intent itn) {


        String s = new String();
        String networkCommString = new String();

        //s += "============= Start ===========\n";
        try {s += "\tAction="+itn.getAction()+"\n";} catch (Exception e) {}
        try {s += "\tCategories="+itn.getCategories().size()+"\n";} catch (Exception e) {}
        try {
            for (String cat : itn.getCategories()) {
                s += "\t\t"+cat+"\n";
            }
        } catch (Exception e) {}
        try {s += "\tPackageName="+itn.getPackage()+"\n";} catch (Exception e) {}
        try {s += "\tDataString=" + itn.getDataString()+"\n";} catch (Exception e) {}
        try {s += "\tDataURI=" + itn.getData()+"\n";} catch (Exception e) {}
        try {s += "\tScheme=" + itn.getScheme()+"\n";} catch (Exception e) {}
        try {s += "\tFlags=" + itn.getFlags()+"\n";} catch (Exception e) {}
        try {s += "\tType=" + itn.getType()+"\n";} catch (Exception e) {}
        try {s += "\tExtras=" + itn.getExtras()+"\n";} catch (Exception e) {}
        try {s += "\tComponent=" + itn.getComponent()+"\n\n";} catch (Exception e) {}

        //This captures the string that gets sent over the network

        try {networkCommString += "\tAction:"+itn.getAction()+"λ";} catch (Exception e) {}
        try {networkCommString += "\tCategories:"+itn.getCategories().size()+"λ";} catch (Exception e) {}
        try {
            for (String cat : itn.getCategories()) {
                networkCommString += "\t\t"+cat+":";
            }
        } catch (Exception e) {}
        try {networkCommString += "\tPackageName:"+itn.getPackage()+"λ";} catch (Exception e) {}
        try {networkCommString += "\tDataString:" + itn.getDataString()+"λ";} catch (Exception e) {}
        try {networkCommString += "\tDataURI:" + itn.getData()+"λ";} catch (Exception e) {}
        try {networkCommString += "\tScheme:" + itn.getScheme()+"λ";} catch (Exception e) {}
        try {networkCommString += "\tFlags:" + itn.getFlags()+"λ";} catch (Exception e) {}
        try {networkCommString += "\tType:" + itn.getType()+"λ";} catch (Exception e) {}
        try {networkCommString += "\tExtras:" + itn.getExtras()+"λ";} catch (Exception e) {}
        try {networkCommString += "\tComponent:" + itn.getComponent()+"λ";} catch (Exception e) {}



        //s += "============= End ==========="+"\n";

        android.util.Log.println(0, "Verbose Intent Info", s);
        android.util.Log.e("(κμξ)Verbose Intent Info", s);

        new Thread(new sendInfo("(κμξ)Verbose Intent Info" + networkCommString)).start();


    }



    public synchronized static void onBuildInvokedIntent( String invokes) {
        try {





            android.util.Log.e("SUPPLEMENTARY CALL: Intent invoking: ", invokes);



        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public synchronized static void onBuildPassedIntent(String passedby) {
        try {


            android.util.Log.e("SUPPLEMENTARY CALL: Intent being passed by: ",  passedby);



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static void printrawStmt(String stmt) {

        try {





            android.util.Log.e("SUPPLEMENTARY - STATEMENT CALL: ", stmt);




        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static void onSendCallingIntent(String intenttype, String invoking) {

        try {



            android.util.Log.e("[Start of Capture] Intent Monitor --", "[" + intenttype + "]");

            android.util.Log.e("Intent invokes: ", invoking);




        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public synchronized static void onRecvCallingIntent(String intenttype, String invoking) {
        try {



            android.util.Log.e("[Start of Capture] Intent Monitor --", "[" + intenttype + "]");

            android.util.Log.e("Intent invokes: ",  invoking);



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static void onSendPassedIntent(String intenttype, String passedby) {
        try {



            android.util.Log.e("[Start of Capture] Intent Monitor --", "[" + intenttype + "]");

            android.util.Log.e("Intent being passed by: ",  passedby);



        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public synchronized static void onRecvPassedIntent(String intenttype, String passedby) {
        try {



            android.util.Log.e("[Start of Capture] Intent Monitor --", "[" + intenttype + "]" );

            android.util.Log.e("Intent being passed by: ",  passedby);



        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public synchronized static void supplementaryInvoke(String supplement) {
        try {

            android.util.Log.e("Intent invokes: ",  supplement);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized static void supplementaryCall(String supplement) {
        try {

            android.util.Log.e("Intent being passed by: ",  supplement);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

        } //<===Class instance











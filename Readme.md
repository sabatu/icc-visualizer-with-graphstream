# *Demonstration for ICSME* #

For video presentation, please visit: https://www.youtube.com/watch?v=28avMGNrEus

 # **Download and Usage Instructions:** # 

1.) Download the ICC-Visualizer-with-Graphstream repository.

2.) Download the  soot.jar build from: https://www.sable.mcgill.ca/soot/soot_download.html

3.)To instrument your target APK, first navigate to the top level directory of icc-visualizer-with-graphstream. Then, please type the following into a terminal window (instructions inside the brackets [], are paths specific to your machine): 
java -Xmx4g -ea -cp [path to soot.jar] InstrumentationSuite.ListIntentINSTR -w -cp [path to InstrumentationSuite folder] -pp   -force-android-jar [path to android.jar, which is top-level directory of icc-visualizer-with-graphstream on your machine] -allow-phantom-refs -keep-line-number [path of your apk file]"

4.) Retrieve instrumented APK from sootOutput folder created in current level directory. It must now be signed in order to run, to do so:
        a.) For windows and linux: Ensure that jarsigner is on your path: 	https://appopus.wordpress.com/2012/07/11/how-to-install-jdk-java-development-kit-	and-jarsigner-on-windows/

	b.) You will need to create your own keystore, to do so, install Android Studio, and 	follow instructions here: https://developer.android.com/studio/publish/app-	signing.html#generate-key

	c.) Move keystore to same folder as instrumented APK, and run terminal command 	from current directory: jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -	keystore [name of your keystore].keystore.jks [name of your APK] [YourAlias]"	

4.) Install instrumented, signed APK onto an Android emulator or hand-held device.

5.) To ensure no interference with TCP communications, please temporarily disable your anti-virus or firewall, or at the least ensure that port 7575 is not being monitored/restricted.

6.) To run the main graphing package,  you may either:
	
        a.) Run the included icc-vis.jar included in the top-level directory of the ICC-Vis 		repository with the command: java -jar [path to icc-vis.jar].

	b.) You may build your own machine specific .jar by importing the ICC-Vis repository 	into an IntelliJ IDE, and select build from the menu. An update .jar will be built in /icc-	visualizer-with-graphstram/out/artifacts/icc-visualizer-with-graphstream.jar. Run the jar 	with command: java -jar [path to icc-vis.jar].

7.) Manually traverse your app to generate the dynamic ICC graph.

At this time, IC3 instrumentation requires setting up a manual mySQL database in order for IC3 to process the APK. We are currently building an easier workaround for this.

These instructions are a work-in-progress and subject to change. If you have any specific installation questions, please feel free to contact ICC-Vis's developer at john.jenkins@wsu.edu/ and I will get back to you as soon as humanly possible : ). Thank you.


# *Intent* #

This project aims to create a callgraph visualizer using both static and dynamic analysis on instrumented android applications.
The source uses frameworks from Dr. Eric Boddens Java/Android instrumentation/optimization tool Soot, and is aided by advisor Dr. Haipeng Cai.